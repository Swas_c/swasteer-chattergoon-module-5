Import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'firebase_options.dart';

await Firebase.initializeApp(
  options: DefaultFirebaseOptions.currentPlatform,
);
flutterfire configure --project=mod5-2c6c6
flutter pub add firebase_database

Project-level build.gradle (<project>/build.gradle):
buildscript {
  dependencies {
    // Add this line
    classpath 'com.google.gms:google-services:4.2.0'
  }
}
App-level build.gradle (<project>/<app-module>/build.gradle):
dependencies {
  // Add this line
  implementation 'com.google.firebase:firebase-core:17.0.0'
}
...
// Add to the bottom of the file
apply plugin: 'com.google.gms.google-services'




void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Welcome to Flutter',
      home: Scaffold(
        appBar: AppBar(
          title: const Text(' Flutter app'),
        ),
        body: const Center(
          child: Text('flutyer app'),
        ),
      ),
    );
    TextField(
  decoration: InputDecoration(
    border: OutlineInputBorder(),
    hintText: 'Enter text',
  ),
),
TextField(
  decoration: InputDecoration(
    border: OutlineInputBorder(),
    hintText: 'Enter text',
  ),
),
TextField(
  decoration: InputDecoration(
    border: OutlineInputBorder(),
    hintText: 'enter text',
  ),
),
  }
}